package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean nextPlay = true;
    String userInput = "";
    String playerHandChoice = "paper";
    String winner = "";
    String computerChoice = "";
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round " + roundCounter);

        while( nextPlay == true) {

            chooseHand();
            computerChoose();

            System.out.println("Human chose " + playerHandChoice + " computer chose " + computerChoice);
            
            checkWinner();
            roundCounter += 1;
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            continuePlay();

            if (nextPlay == false) {
                System.out.println("Bye bye :)");
                break;
            }
            
            System.out.println("Let's play round " + roundCounter);
        
        }
    }



    public void continuePlay() {

        String choice = sc.nextLine();
        if (choice.equals("y"))
            nextPlay = true;

            else if (choice.equals("n"))  
            nextPlay = false;  
    
    }

    public String chooseHand(){

        System.out.println("Your choice (Rock/Paper/Scissors)?");
        String myHand = sc.nextLine();

        if (myHand.contains("rock")) {
        playerHandChoice = "rock";
        }

        if (myHand.contains("paper")){
        playerHandChoice = "paper";
        }

        if (myHand.contains("scissor")) {
        playerHandChoice = "scissor";
    }
    return playerHandChoice;
        
    
    }
    
    public void computerChoose(){
        
        int computerChance = (int)(Math.random()*3);

        if (computerChance == 0)
        {computerChoice = "rock"; }

        if (computerChance == 1)
        {computerChoice = "paper"; }

        if (computerChance == 2)
        {computerChoice = "scissor"; }

    }

    public String checkWinner() {

        if ( playerHandChoice == computerChoice)
        {
            humanScore += 0;
            computerScore += 0;
            winner = "It's a tie!";
        }

        if ( playerHandChoice.equals("rock") && computerChoice.equals("scissor"))
        humanScore += 1;
        winner = "Human wins!";

        if ( playerHandChoice.equals("paper") && computerChoice.equals("rock"))
        humanScore += 1;
        winner = "Human wins!";

        if ( playerHandChoice.equals("scissor") && computerChoice.equals("paper"))
        humanScore += 1;
        winner = "Human wins!";

        if ( computerChoice.contains("rock") && playerHandChoice.contains("scissor"))
        computerScore += 1;
        winner = "Computer wins!";

        if ( computerChoice.contains("paper") && playerHandChoice.contains("rock"))
        computerScore += 1;
        winner = "Computer wins!";

        if ( computerChoice.contains("scissor") && playerHandChoice.contains("paper"))
        computerScore += 1;
        winner = "Computer wins!";

        return winner;
        


    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
